#include <entry_point.hpp>
#include <ge/utils/string.hpp>
#include <hashPassword.hpp>

int run(IConnection& c, ge::Logger& logger, ge::core::Engine& engine)
{
    logger.log("Hello menu");

    const std::string username = "SuperFola",
        password = "password!";

    auto hp = hashPassword(username + password);  // on doit hasher username+password d'après le cahier des charges ;-)
    logger.log("{0} produced {1} ({3}) with salt {2}"s, hp.password, hp.hash, hp.salt, hp.hash.size());
    auto new_hash = generateHashPprime(username, password, hp.salt);  // on devra stocker le salt dans un fichier plus tard
    logger.log("{0} == {1} : {2}"s, hp.hash, new_hash, new_hash == hp.hash);

    // auto id = engine.addScene<MyScene>(/* test_arg */ 12, logger, &c);
    // engine.setCurrentScene(id);

    logger.log("Goodbye menu");
    return 1;  // launch game
}