#ifndef menu_haspassword_hpp
#define menu_haspassword_hpp

#include <argon2.h>
#include <cinttypes>
#include <string>
#include <random>
#include <vector>
#include <chrono>

struct HashedPassword
{
    std::string password;
    std::string hash;
    std::string salt;
};

// à utiliser quand on veut générer un hash
inline HashedPassword hashPassword(const std::string& password, const std::string& salt_="", std::size_t hashLen=32, std::size_t saltLen=16,
    uint32_t timeCost=1, uint32_t memCost=1<<16, uint32_t threadCount=1)
{
    HashedPassword hp;
    hp.password = password;

    uint8_t* hash = new uint8_t[hashLen];
    uint8_t* salt = new uint8_t[saltLen];

    // generate random salt
    if (salt_ == "")
    {
        std::mt19937 gen((unsigned) std::chrono::system_clock::now().time_since_epoch().count());
        for (std::size_t i=0; i < saltLen; ++i)
            salt[i] = (uint8_t) ((gen() % ('~' - ' ' + 1)) + ' ');
    }
    else
    {
        for (std::size_t i=0; i < salt_.size(); ++i)
            salt[i] = salt_[i];
    }

    hp.salt = std::string((char*) salt);

    uint8_t* pwd = new uint8_t[password.size()];
    uint32_t pwdLen = password.size();
    for (std::size_t i=0; i < password.size(); ++i)
        pwd[i] = (uint8_t) password[i];

    // high level API
    argon2i_hash_raw(
        timeCost, memCost, threadCount,
        pwd,  pwdLen,
        salt, saltLen,
        hash, hashLen
    );

    
    hp.hash = std::string((char*) hash);

    return hp;
}

// à utiliser quand on connait le salt
inline std::string generateHashPprime(const std::string& username, const std::string& password, const std::string salt)
{
    return hashPassword(username + password, salt).hash;
}

#endif