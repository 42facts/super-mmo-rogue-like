#ifndef menu_entry_point_hpp
#define menu_entry_point_hpp

#include <IConnection.hpp>
#include <ge/logger.hpp>
#include <ge/core/engine.hpp>

using namespace std::string_literals;

extern "C" int run(IConnection&, ge::Logger&, ge::core::Engine&);

#endif