cmake_minimum_required(VERSION 3.8)

project(
    Menu
    LANGUAGES
        CXX C
    VERSION
        1.0.0
    DESCRIPTION
        "Menu code"
)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include_directories(
    ${PROJECT_SOURCE_DIR}/include
    ${PROJECT_SOURCE_DIR}/../../global/include
    ${PROJECT_SOURCE_DIR}/../../thirdparty
    ${PROJECT_SOURCE_DIR}/../../engine/include
    ${PROJECT_SOURCE_DIR}/../../engine/thirdparty
    ${PROJECT_SOURCE_DIR}/../../engine/thirdparty/lua-5-3-4
    ${PROJECT_SOURCE_DIR}/../../argon2/include
)

file(GLOB_RECURSE SOURCE_FILES
    ${PROJECT_SOURCE_DIR}/source/*.cpp
    ${PROJECT_SOURCE_DIR}/../../thirdparty/*.cpp
    ${PROJECT_SOURCE_DIR}/../../thirdparty/*.c
    ${PROJECT_SOURCE_DIR}/../../global/source/*.cpp
)

add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES})

target_link_libraries(${PROJECT_NAME} PRIVATE
    GameEngine2
    Argon2
)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(${PROJECT_NAME} PRIVATE Threads::Threads)

if (UNIX OR LINUX)
    target_link_libraries(
        ${PROJECT_NAME}
        PRIVATE ${CMAKE_DL_LIBS}
    )
endif()

set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
        CXX_EXTENSIONS OFF
)

add_custom_command(TARGET ${PROJECT_NAME}
    COMMAND ${CMAKE_COMMAND} -E copy
        "$<TARGET_FILE:${PROJECT_NAME}>" ${PROJECT_SOURCE_DIR}/../../launcher
)
