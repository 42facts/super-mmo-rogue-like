# Module menu

Lancé par le Launcher, doit obligatoirement être fourni de base avec le jeu.

Récupère la connexion, l'engine et le logger, et doit gérer:

* la GUI
    * connexion
        * retenir le mot de passe
        * hasher le mdp avec argon2
        * l'envoyer au serveur
    * paramètres
        * graphiques
            * controles
            * FPS
            * vsync
            * taille de la fenetre (nécessitera de redémarrer le jeu)
        * sonores
            * volume sons
        * internet
            * télécharger les nouveaux assets en jeu
* teste de connectivité pour voir si le serveur est joignable

Si le serveur n'est pas joignable, run doit retourner 2
Si le serveur est joignable, et que par extension le joueur s'est connecté, run doit retourner 1.
Si le joueur a juste fermé la fenêtre, run doit retourner 0.