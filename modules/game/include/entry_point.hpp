#ifndef game_entry_point_hpp
#define game_entry_point_hpp

#include <IConnection.hpp>
#include <ge/logger.hpp>
#include <ge/core/engine.hpp>
#include <ge/core/scenes/scene.hpp>
#include <ge/json.hpp>
#include <mutex>
#include <DisplayHeaders.hpp>

using namespace std::string_literals;

class MyScene: public ge::core::scenes::Scene
{
public:
    MyScene(std::size_t id, int test_arg, ge::Logger& logger, IConnection* c) :
        ge::core::scenes::Scene(id, "assets/scripts/test.lua")
        , m_logger(logger)
        , m_c(c)
    {
        m_lua()["myscene_coucou"] = kaguya::function([](std::string s){std::cout << "lua coucou: " << s << std::endl;});

        m_texture.loadFromFile("assets/default.png");
        m_sprite.setTexture(m_texture);
        m_sprite.setPosition(0, 0);

        m_x = m_y = 0;
    }

    ~MyScene()
    {}

    void handleEvent(const sf::Event& event)
    {
        if (event.type == sf::Event::KeyPressed)
        {
            if (event.key.code == sf::Keyboard::Z)
            {
                m_c->post("/play", ge::json("move", "up"), [this](auto res){
                    if (res && res->status == 200)
                    {
                        m_mutex.lock();
                        m_y -= 32;
                        m_mutex.unlock();
                        DisplayHeaders(res, m_logger);
                        m_logger.log("{}"s, res->body);
                    }
                    else
                        m_logger.log("Erreur déplacement, /play a retourné {}"s, (res ? res->status : -1));
                });
            }
            else if (event.key.code == sf::Keyboard::S)
            {
                m_c->post("/play", ge::json("move", "down"), [this](auto res){
                    if (res && res->status == 200)
                    {
                        m_mutex.lock();
                        m_y += 32;
                        m_mutex.unlock();
                    }
                    else
                        m_logger.log("Erreur déplacement, /play a retourné {}"s, (res ? res->status : -1));
                });
            }
            else if (event.key.code == sf::Keyboard::Q)
            {
                m_c->post("/play", ge::json("move", "left"), [this](auto res){
                    if (res && res->status == 200)
                    {
                        m_mutex.lock();
                        m_x -= 32;
                        m_mutex.unlock();
                    }
                    else
                        m_logger.log("Erreur déplacement, /play a retourné {}"s, (res ? res->status : -1));
                });
            }
            else if (event.key.code == sf::Keyboard::D)
            {
                m_c->post("/play", ge::json("move", "right"), [this](auto res){
                    if (res && res->status == 200)
                    {
                        m_mutex.lock();
                        m_x += 32;
                        m_mutex.unlock();
                    }
                    else
                        m_logger.log("Erreur déplacement, /play a retourné {}"s, (res ? res->status : -1));
                });
            }
        }
    }

    void update(sf::Time dt)
    {
        m_mutex.lock();
        m_sprite.setPosition(m_x, m_y);
        m_mutex.unlock();
    }

    void render(sf::RenderTarget& window)
    {
        window.draw(m_sprite);
    }

private:
    ge::Logger& m_logger;
    IConnection* m_c;

    std::mutex m_mutex;

    sf::Sprite m_sprite;
    sf::Texture m_texture;
    int m_x, m_y;
};

extern "C" int run(IConnection&, ge::Logger&, ge::core::Engine&);

#endif