#include <entry_point.hpp>

#include <DisplayHeaders.hpp>

int run(IConnection& c, ge::Logger& logger, ge::core::Engine& engine)
{
    logger.log("game module started");

    c.post("/connect", ge::json("key", "value"), [&] (auto res) {
        logger.log("/connect returned with: {0}"s, (res ? res->status : -1));

        if (res && res->status == 200 && res->has_header("playerId"))
            c.setHeader("playerId", res->get_header_value("playerId"));
    });

    auto id = engine.addScene<MyScene>(/* test_arg */ 12, logger, &c);
    engine.setCurrentScene(id);
    engine.run();

    c.waitPost("/disconnect", ge::json("key", "value"), [&](auto res){
        logger.log("/disconnect returned with: {0}"s, (res ? res->status : -1));
    });

    return 0;
}