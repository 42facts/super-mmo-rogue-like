#ifndef game_gettime
#define game_gettime

#include <chrono>
#include <ctime>
#include <string>

namespace game
{
    inline std::string getTime()
    {
        auto now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);

        std::string temp = std::ctime(&t);
        std::string out = "";
        bool last_was_tiret = false;

        for (std::size_t i=0; i < temp.size(); ++i)
        {
            if (temp[i] != '\'' && temp[i] != '\n' && temp[i] != '$')
            {
                if (temp[i] != ' ')
                {
                    out += temp[i];
                    last_was_tiret = false;
                }
                else if (temp[i] == ' ' && !last_was_tiret)
                {
                    out += '-';
                    last_was_tiret = true;
                }
            }
        }
        
        return out;
    }
}

#endif