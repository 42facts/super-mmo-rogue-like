#include <IConnection.hpp>
#include <game/GetTime.hpp>
#include <ge/logger.hpp>
#include <ge/config/loader.hpp>
#include <string>
#include <ge/json.hpp>
#include <ge/core/plugin.hpp>
#include <ge/constants.hpp>
#include <ge/core/engine.hpp>

#define LOG_TO_TERMINAL
#define SHOULD_BE_LOGGING

int main()
{
    using namespace std::string_literals;

    // setting up logger
#ifdef LOG_TO_TERMINAL
    ge::Logger logger;
#else
    ge::Logger logger(/* filename */ "Log-" + game::getTime() + ".log");
#endif

#ifdef SHOULD_BE_LOGGING
    logger.setLevel(ge::LogLevel::Normal);
#else
    logger.setLevel(ge::LogLevel::Dont);
#endif

    logger.info("Starting launcher");

    // load config file
    ge::config::Loader config("game.cfg");
    logger.success("'game.cfg' loaded");

    // create connection
    IConnection c(
        config()["server"]["host"].asString().c_str()
      , static_cast<int>(config()["server"]["port"].asInt())
    );
    logger.success(
        "client connected to server {0}:{1}"s
      , config()["server"]["host"].asString().c_str()
      , static_cast<int>(config()["server"]["port"].asInt())
    );

    // c.setMainRoute("/mmo");

    // create engine
    ge::core::Engine engine(
        static_cast<int>(config()["settings"]["window"]["width"].asInt())
      , static_cast<int>(config()["settings"]["window"]["height"].asInt())
    );
    engine.setImGUIDebug(true);
    engine.setTitle("Super MMO Rogue Like");
    engine.setVSync(static_cast<bool>(config()["settings"]["window"]["vsync"].asBool()));
    engine.setFPSLimit(static_cast<int>(config()["settings"]["window"]["fps limit"].asInt()));

    // load menu
    ge::core::SharedLibrary module_menu;
#if defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
    module_menu.load("./libMenu.so");
#elif defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
    module_menu.load("./menu.dll");
#endif

    auto run_menu = module_menu.get<int (*)(IConnection&, ge::Logger&, ge::core::Engine&)>("run");
    int return_code = run_menu(c, logger, engine);

    // refer to modules/menu/documentation/main.md
    switch (return_code)
    {
        case 0:
            break;
        
        case 1:
        {
            logger.log("Launching game");

            // loading game
            ge::core::SharedLibrary module_game;
#if defined(GE_SYS_UNIX) || defined(GE_SYS_APPLE)
            module_game.load("./libGame.so");
#elif defined(GE_SYS_WIN32) || defined(GE_SYS_WIN64)
            module_game.load("./game.dll");
#endif
            auto run_game = module_game.get<int (*)(IConnection&, ge::Logger&, ge::core::Engine&)>("run");
            run_game(c, logger, engine);

            break;
        }
        
        case 2:
            logger.error("Couldn't join server");
            break;
        
        default:
            logger.warn("Unknown return code: {}"s, return_code);
            break;
    }

    logger.info("Closing launcher");
    return 0;
}