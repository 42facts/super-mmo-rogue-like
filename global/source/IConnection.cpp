#include <IConnection.hpp>

IConnection::IConnection(const char* host, int port) :
    m_mainRoute("")
    , m_client(host, port)
{}

IConnection::~IConnection()
{}

void IConnection::setMainRoute(const std::string& mainRoute)
{
    m_mainRoute = mainRoute;
}

void IConnection::post(const std::string& route, const std::string& msg, IConnection::Callback_t callback)
{
    std::thread t([this, route, msg] (IConnection::Callback_t&& callback) -> void {
        m_mutex.lock();

        if (!m_headers.empty())
        {
            auto res = m_client.Post((m_mainRoute + route).c_str(), m_headers, msg, "application/json");
            callback(res);
        }
        else
        {
            auto res = m_client.Post((m_mainRoute + route).c_str(), msg, "application/json");
            callback(res);
        }

        m_mutex.unlock();
    }, std::move(callback));

    t.detach();
}

void IConnection::get(const std::string& route, IConnection::Callback_t callback)
{
    std::thread t([this, route] (IConnection::Callback_t&& callback) -> void {
        m_mutex.lock();

        if (!m_headers.empty())
        {
            auto res = m_client.Get((m_mainRoute + route).c_str(), m_headers);
            callback(res);
        }
        else
        {
            auto res = m_client.Get((m_mainRoute + route).c_str());
            callback(res);
        }

        m_mutex.unlock();
    }, std::move(callback));

    t.detach();
}

void IConnection::waitPost(const std::string& route, const std::string& msg, IConnection::Callback_t callback)
{
    m_mutex.lock();

    if (!m_headers.empty())
    {
        auto res = m_client.Post((m_mainRoute + route).c_str(), m_headers, msg, "application/json");
        callback(res);
    }
    else
    {
        auto res = m_client.Post((m_mainRoute + route).c_str(), msg, "application/json");
        callback(res);
    }

    m_mutex.unlock();
}

void IConnection::waitGet(const std::string& route, IConnection::Callback_t callback)
{
    m_mutex.lock();

    if (!m_headers.empty())
    {
        auto res = m_client.Get((m_mainRoute + route).c_str(), m_headers);
        callback(res);
    }
    else
    {
        auto res = m_client.Get((m_mainRoute + route).c_str());
        callback(res);
    }

    m_mutex.unlock();
}

void IConnection::setHeader(const std::string& name, const std::string& value)
{
    m_headers.insert(name, value);
}