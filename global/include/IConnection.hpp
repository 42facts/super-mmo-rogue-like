#ifndef global_iconnection_hpp
#define global_iconnection_hpp

#include <httplib.hpp>
#include <functional>
#include <string>
#include <memory>
#include <mutex>

class IConnection
{
public:
    using Callback_t = std::function<void(std::shared_ptr<httplib::Response> res)>;

    IConnection(const char* host, int port);
    ~IConnection();

    void setMainRoute(const std::string& mainRoute);
    
    void post(const std::string& route, const std::string& msg, IConnection::Callback_t callback);
    void get(const std::string& route, IConnection::Callback_t callback);
    
    void waitPost(const std::string& route, const std::string& msg, IConnection::Callback_t callback);
    void waitGet(const std::string& route, IConnection::Callback_t callback);

    void setHeader(const std::string& name, const std::string& value);

private:
    std::string m_mainRoute;
    httplib::Client m_client;
    std::mutex m_mutex;
    httplib::Headers m_headers;
};

#endif