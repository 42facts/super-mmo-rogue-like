#ifndef global_displayheaders_hpp
#define global_displayheaders_hpp

#include <vector>
#include <string>
#include <httplib.hpp>
#include <ge/logger.hpp>

using namespace std::string_literals;

const std::vector<std::string> CommonHTTPHeaders = {
    "www-authenticate",
    "authorization",
    "proxy-authenticate",
    "proxy-authorization",
    "age",
    "cache-control",
    "clear-site-data",
    "expires",
    "pragma",
    "warning",
    "accept-ch",
    "accept-ch-lifetime",
    "early-data",
    "save-data",
    "viewport-width",
    "width",
    "last-modified",
    "etag",
    "vary",
    "connection",
    "keep-alive",
    "accept",
    "accept-charset",
    "accept-encoding",
    "accept-language",
    "expect",
    "max-forwards",
    "cookie",
    "set-cookie",
    "cookie2",
    "set-cookie2",
    "access-control-allow-origin",
    "access-control-allow-credentials",
    "access-control-allow-headers",
    "access-control-allow-methods",
    "access-control-expose-headers",
    "access-control-max-age",
    "access-control-request-headers",
    "access-control-request-method",
    "cross-origin-resource-policy",
    "origin",
    "timing-allow-origin",
    "x-permitted-cross-domain-policies",
    "dnt",
    "tk",
    "content-length",
    "content-type",
    "content-encoding",
    "content-language",
    "content-location",
    "location",
    "from",
    "host",
    "referer",
    "referer-policy",
    "user-agent",
    "allow",
    "server"
};

void DisplayHeaders(std::shared_ptr<httplib::Response> res, ge::Logger& logger)
{
    for (auto header : CommonHTTPHeaders)
    {
        if (res->has_header(header.c_str()))
        {
            for (std::size_t i=0; i < res->get_header_value_count(header.c_str()); ++i)
                logger.log("[{0}] {1} => {2}"s, i, header, res->get_header_value(header.c_str()));
        }
    }
}

#endif