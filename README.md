# Super MMO Rogue Like

## Game Design Document

[Here!](https://bimestriel.framapad.org/p/paxl0chhtp-superroguelike)

## Dependencies

* C++17 compiler
* CMake 3.8
* GameEngine2
    * SFML 2.5.x (must be installed on Linux)
    * JSON cpp
    * EnTT
    * ryjen fmt
    * clipp
    * kaguya
    * Lua 5.3.4
    * termcolor
    * SFML imgui
* httplib
* gason
* Argon2

## Building

```bash
# update submodules
$ git submodule update --init --recursive
# force update
$ ./update

# on Linux
$ cmake -H. -Bbuild  # use -DCMAKE_BUILD_TYPE=Debug to generate debug symbols
# on Windows
$ cmake -H. -Bbuild -G "Visual Studio 15 Win64"

$ cmake --build build
```

## Launching

```bash
$ cd launcher
$ ./launcher  # temporary
```